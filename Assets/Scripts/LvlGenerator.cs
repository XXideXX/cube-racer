using System.Collections.Generic;
using UnityEngine;

public class LvlGenerator : MonoBehaviour
{

    [SerializeField] private GameObject tile;
    [SerializeField] private GameObject player;

    [SerializeField] private int radius = 5;
    [SerializeField] private int planeOffset = 10;

    [SerializeField] private Transform tileParrent;
    private Vector3 startPos = Vector3.zero;

    [Header("obstacle")]
    [SerializeField] private List<GameObject> prefabs = new List<GameObject>();
    [SerializeField] private int splitMultiplayer = 10;
    [SerializeField] private Transform obsticaleParent;

    private int xPlayerMove => (int)(player.transform.position.x- startPos.x);
    private int zPlayerMove => (int)(player.transform.position.z - startPos.z);

    private int xPlayeLocation => (int)Mathf.Floor(player.transform.position.x / planeOffset) * planeOffset;
    private int zPlayeLocation => (int)Mathf.Floor(player.transform.position.z / planeOffset) * planeOffset;

    // private Hashtable tilePlane = new Hashtable();

   [SerializeField] private PositionListController positionList;
    private void Start()
    {
        if (startPos == Vector3.zero)
        {
            for (int x = -radius; x < radius; x++)
            {
                for (int z = -radius; z < radius; z++)
                {
                    Vector3 pos = new Vector3((x * planeOffset + xPlayeLocation), 0f
                        , (z * planeOffset + zPlayeLocation));
                    if (!positionList.position.Contains(pos))
                    {

                        GameObject _tile = Instantiate(tile, pos, Quaternion.identity);
                        _tile.transform.parent = tileParrent;
                        positionList.position.Add(pos);

                    }
                }
            }
        }
    }

    private void Update()
    {
        if (startPos == Vector3.zero)
        {
            for (int x = -radius; x < radius; x++)
            {
                for (int z = -radius; z < radius; z++)
                {
                    Vector3 pos = new Vector3((x * planeOffset + xPlayeLocation), 0f
                        , (z * planeOffset + zPlayeLocation));

                    if (!positionList.position.Contains(pos))
                    {

                        GameObject _tile = Instantiate(tile, pos, Quaternion.identity);
                        CreateObstacle(pos);
                        _tile.transform.parent = tileParrent;
                        positionList.position.Add(pos);

                    }
                }
            }
        }
      

        if (hasPlayerMoved())
        {

          
                for (int x = -radius; x < radius; x++)
                {
                    for (int z = -radius; z < radius; z++)
                    {
                        Vector3 pos = new Vector3((x * planeOffset + xPlayeLocation), 0f
                            , (z * planeOffset + zPlayeLocation));

                        if (!positionList.position.Contains(pos))
                        {
                       
                            GameObject _tile = Instantiate(tile, pos, Quaternion.identity);
                             CreateObstacle(pos);
                             _tile.transform.parent = tileParrent;
                             positionList.position.Add(pos);

                        }
                    }
                }
            

        }


    }
    private void CreateObstacle(Vector3 pos)
    {
        GameObject obsticale = Instantiate(prefabs[Random.Range(0, prefabs.Count - 1)],
           pos + new Vector3(Random.value * splitMultiplayer, 0, Random.value * splitMultiplayer),
            Quaternion.Euler(0, GetPosition(), 0));
        obsticale.transform.parent = obsticaleParent;
    }
    private int GetPosition()
    {
        int[] obsticleRotation = new int[]
        {
            -45,
            0,
            90,
            45,
        };
        int pos = obsticleRotation[Random.Range(0, obsticleRotation.Length - 1)];

        return pos;
    }
    bool hasPlayerMoved()
    {

        if (Mathf.Abs(xPlayerMove) >= planeOffset || Mathf.Abs (zPlayerMove)>= planeOffset)
        {
            return true;
        }
        return false;
    }


}
