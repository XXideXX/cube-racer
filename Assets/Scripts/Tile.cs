using System.Collections.Generic;
using UnityEngine;

public class Tile : Removable
{
    
   
    [SerializeField] private PositionListController positionList;
 
 

    protected override void OnTriggerExit(Collider other)
    {
        Vector3 position = gameObject.transform.position;

        base.OnTriggerExit(other);
       
        positionList.position.Remove(position);
    }
 


}
