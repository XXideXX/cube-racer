using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Removable : MonoBehaviour
{
    protected virtual void OnTriggerExit(Collider other)
    {
        Destroy(gameObject);
    }
}
