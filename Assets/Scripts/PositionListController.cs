using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PositionList", menuName = "ScriptableObjects/SpawnManagerScriptableObject")]
public class PositionListController : ScriptableObject
{
    public List<Vector3> position = new List<Vector3>();
    private void OnEnable()
    {
        // ���� ��������
        position.Clear();
    }
 

}
