using UnityEngine;

public class TouchController : MonoBehaviour
{
    [HideInInspector]public float pointerX ;
 
    [SerializeField]private float sensativityX;

    private void Awake()
    {
        pointerX = Input.GetAxis("Mouse X");
   
    }


    void Update()
    {

        if (Input.touchCount > 0)
        {
            pointerX = Input.touches[0].deltaPosition.x * sensativityX;
       

            //  Debug.Log(pointerX);
            // Debug.Log(pointerY);
        }
        else
        {
            pointerX = 0;
           
        }
     

    }
}
