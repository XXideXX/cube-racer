using UnityEngine;

public class cameraController : MonoBehaviour
{
    [SerializeField] private Transform camera;
    private Vector3 cameraStartingPos;
    [SerializeField] private Vector3 offset;
    private void Start()
    {
        cameraStartingPos = camera.position;
    }

    void LateUpdate()
    {
        camera.position = offset+ cameraStartingPos +gameObject.transform.position ;
        

    }
}
