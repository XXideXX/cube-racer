using UnityEngine;

public class CarController : MonoBehaviour
{
   
   [SerializeField] private TouchController touchController;

   [SerializeField] private float MoveSpeed = 50;
   [SerializeField] private float MaxSpeed = 15;

   [SerializeField] private float Drag = 0.98f;
   [SerializeField] private float rotationMultiplayer = 20;

   [SerializeField] private float Traction = 1;

  
    private Vector3 MoveForce;


    void Update()
    {

        MoveForce +=  new Vector3 (0,0,transform.forward.z) * MoveSpeed * Time.deltaTime;
        transform.position += MoveForce * Time.deltaTime;

        MoveForce *= Drag;
        MoveForce = Vector3.ClampMagnitude(MoveForce, MaxSpeed);

        transform.Rotate(Vector3.up * touchController.pointerX * MoveForce.magnitude * rotationMultiplayer * Time.deltaTime);


        transform.position += (transform.right * touchController.pointerX * MoveForce.magnitude * rotationMultiplayer * Time.deltaTime);
       

        Debug.DrawRay(transform.position, MoveForce.normalized * 3);
        Debug.DrawRay(transform.position, transform.forward * 3, Color.blue);
        MoveForce = Vector3.Lerp(MoveForce.normalized, transform.forward, Traction * Time.deltaTime) * MoveForce.magnitude;
    }
}
